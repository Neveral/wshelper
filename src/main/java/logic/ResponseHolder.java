package logic;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

public class ResponseHolder {
    private List<String> columns = new ArrayList<>();
    private List<List<String>> rows = new ArrayList<>();


    public void addColumn(String column){
        columns.add(column);
    }

    public void addRow(List row){
        rows.add(row);
    }

    public int getSize(){
        return rows.size();
    }


    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < rows.size(); i++) {
            for (int j = 0; j < rows.get(0).size(); j++) {
                str.append(Matcher.quoteReplacement(rows.get(i).get(j)) + "\t");
            }
            if(i != rows.size()-1) // add lineseparator after all rows besides last row
                str.append(System.lineSeparator());
        }
        return str.toString();
    }
}
