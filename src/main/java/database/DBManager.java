package database;

import gui.MainFrame;
import logic.ResponseHolder;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class DBManager {

    private static Connection connection;
    private String query_body = "SELECT agent_log.timestamp, agent.description, tnx.name, page.name, request.orderno, agent_log.data \n" +
            "from agent_log \n" +
            "left join runtime_transaction ON agent_log.tnx = runtime_transaction.id \n" +
            "left join tnx ON runtime_transaction.tnx = tnx.id \n" +
            "left join agent ON agent_log.id = agent.id \n" +
            "left join request ON agent_log.request = request.id \n" +
            "left join page ON request.page = page.id \n" +
            "where agent_log.test = ? ";

    public DBManager() {
        this.connection = DBConnection.getConnection();
    }

    public int cleanTable(String tableName) {
        int result = -1;
        try (Statement statement = connection.createStatement()) {
            result = statement.executeUpdate("TRUNCATE " + tableName);
            connection.commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
            MainFrame.getInstance().setStatus(ex.getMessage());
        }
        return result;
    }


    public ResponseHolder getLog(String testID) {
        ResponseHolder responseHolder = null;
        try {
            String query = query_body + "ORDER BY timestamp";
            //PreparedStatement preparedStatement = connection.prepareStatement("SELECT timestamp, data FROM agent_log WHERE test = ?");
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setString(1, testID);
            ResultSet resultSet = preparedStatement.executeQuery();
            responseHolder = getResponseFromDB(resultSet);

            resultSet.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            MainFrame.getInstance().setStatus(ex.getMessage());
        }

        return responseHolder;
    }

    public ResponseHolder getLogWithParameters(String testID, String searchedText, boolean isInclude) {

        ResponseHolder responseHolder = null;
        String query;
        searchedText = new StringBuilder("%" + searchedText + "%").toString();

        if(isInclude == true)
            query = query_body +
                    "AND data like ? \n" +
                    "ORDER BY timestamp ";
        else
            query = query_body +
                    "AND data NOT like? \n" +
                    "ORDER BY timestamp ";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, testID);
            preparedStatement.setString(2, searchedText);
            ResultSet resultSet = preparedStatement.executeQuery();
            responseHolder = getResponseFromDB(resultSet);

            resultSet.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            MainFrame.getInstance().setStatus(ex.getMessage());
        }

        return responseHolder;
    }



    private String parseTimeStamp(String timeStamp){

        StringBuilder str = new StringBuilder();
        str.append(timeStamp.substring(0, 4)).append("/").append(timeStamp.substring(4,6)).append("/").append(timeStamp.substring(6,8)); // parse Date
        str.append(" "); // delemiter between Data and Time

        int hours = (Integer.parseInt(timeStamp.substring(8,10)) + 4) % 24; // fix worksoft bug (His time in DB is incorrect (time lags for 4 hours))
        str.append(hours).append(":").append(timeStamp.substring(10,12)).append(":").append(timeStamp.substring(12, 14)); // parse time

        return str.toString();
    }

    private ResponseHolder getResponseFromDB(ResultSet resultSet){
        // add data from ResultSet to ResponseHolder class (more comfortable to parse in future)
        ResponseHolder responseHolder = new ResponseHolder();

        try {
            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            int columnCount = resultSetMetaData.getColumnCount();

            for (int i = 1; i <= columnCount; i++) {
                responseHolder.addColumn(resultSetMetaData.getColumnName(i));
            }

            List row;
            while (resultSet.next()) {
                row = new ArrayList();
                for (int index = 1; index <= columnCount; index++) {
                    String str = resultSet.getString(index);
                    if (str == null)
                        str = new String("NULL");
                    str = str.replaceAll("\\n", ""); // clean all whitespace symbols
                    if (resultSetMetaData.getColumnName(index).equals("timestamp"))
                        str = parseTimeStamp(str);
                    row.add(str);
                }
                responseHolder.addRow(row);
            }
        }catch(SQLException ex){
            ex.printStackTrace();
            MainFrame.getInstance().setStatus(ex.getMessage());
        }

        return responseHolder;
    }
}
