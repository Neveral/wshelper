package database;

import gui.MainFrame;

import javax.swing.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection {
    public static Connection getConnection() {
        Properties props = new Properties();
        Connection con = null;
        try {
            try(FileInputStream fis = new FileInputStream("db.properties")){
                props.load(fis);
            }catch(FileNotFoundException ex){
                ex.printStackTrace();
                System.err.println("db.properties file does not found");
                JPanel panel = new JPanel();
                JOptionPane.showMessageDialog(panel, "db.properties file does not found", "Error", JOptionPane.ERROR_MESSAGE);
                System.exit(0);
            }



            // load the Driver Class
            Class.forName(props.getProperty("DB_DRIVER_CLASS"));

            // create the connection now
            //autoReconnect=true&
            con = DriverManager.getConnection(props.getProperty("DB_URL")+"?relaxAutoCommit=true",
                    props.getProperty("DB_USERNAME"),
                    props.getProperty("DB_PASSWORD"));
        } catch (IOException | ClassNotFoundException | SQLException e) {
            // TODO Auto-generated catch block
            System.err.println("There are some troubles with connection to DB: check db.properties file");
            e.printStackTrace();
            JPanel panel = new JPanel();
            JOptionPane.showMessageDialog(panel, "There are some troubles with connection to DB: check db.properties file", "Error", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
        return con;
    }
}
