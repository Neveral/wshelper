package gui;

import database.DBManager;
import logic.ResponseHolder;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainFrame extends JFrame {

    private static MainFrame instance;
    private JPanel mainPanel;
    private JLabel statusLabel;
    private JTextField testIDField;
    private JTextField searchedTextField;
    private JCheckBox conditionCheckBox;
    private JCheckBox excludeCheckBox;
    private ResponseHolder result;
    private JButton countLogButton;
    private DBManager dbManager = new DBManager();
    private static final String FILE_NAME = "log.txt";


    public static synchronized MainFrame getInstance() {
        if (instance == null)
            instance = new MainFrame();
        return instance;

    }

    private MainFrame() throws HeadlessException {

        super("WSHelper");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setDefaultLookAndFeelDecorated(true);
        Dimension dimension = new Dimension(500, 300);
        setMinimumSize(dimension);
        setPreferredSize(dimension);
        setMaximumSize(new Dimension(600, 300));
        setResizable(false);


        // JMenuBar
        JMenuBar jMenuBar = new JMenuBar();
        JMenu menuAbout = new JMenu("About");
        JMenuItem itemAbout = new JMenuItem("About");

        itemAbout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String log_format = "date/time | agent_name | transaction_name | agent_message | request_name | request_number";
                JOptionPane.showMessageDialog(mainPanel, "Work Soft Helper v2.0 \n\n" +
                        "Create a log in the following format: \n"
                        + log_format, "About", JOptionPane.PLAIN_MESSAGE);
            }
        });
        menuAbout.add(itemAbout);
        jMenuBar.add(menuAbout);
        // END JMEnuBar

        // clean tables panel
        JPanel cleanMetricPanel = new JPanel();
        cleanMetricPanel.setBorder(BorderFactory.createTitledBorder("Metrics"));
        JButton cleanMetricButton = new JButton("Delete all");
        cleanMetricButton.addActionListener(new ClearMetricButtonClick());
        //JButton cleanRuntimeRequestButton = new JButton("Clean runtime requests");
        //cleanRuntimeRequestButton.addActionListener(new CleanRuntimeRequestButtonClick());
        cleanMetricPanel.add(cleanMetricButton);
        //cleanMetricPanel.add(cleanRuntimeRequestButton);
        // end clean tables panel

         /* ------ log panel --------- */
        JPanel logPanel = new JPanel();
        logPanel.setLayout(new BoxLayout(logPanel, BoxLayout.Y_AXIS));

        logPanel.setBorder(BorderFactory.createTitledBorder("Agent log"));

        // ID button and Label panel
        JPanel testIDButtonAndLabelPanel = new JPanel(new BorderLayout(5, 5));
        testIDButtonAndLabelPanel.setBorder(new EmptyBorder(0, 10, 0, 10));
        JLabel testIDLabel = new JLabel("Test ID:");
        testIDLabel.setLabelFor(testIDLabel);
        testIDField = new JTextField();
        testIDButtonAndLabelPanel.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        testIDButtonAndLabelPanel.add(BorderLayout.WEST, testIDLabel);
        testIDButtonAndLabelPanel.add(BorderLayout.CENTER, testIDField);
        // END ID button and Label panel

        // Conditional button and label panel
        JPanel conditionButtonAndLabelPanel = new JPanel(new BorderLayout(5, 5));
        conditionButtonAndLabelPanel.setBorder(new EmptyBorder(0, 2, 0, 10));
        conditionCheckBox = new JCheckBox("Add Filter: ");
        conditionCheckBox.addActionListener(new ConditionCheckBoxClick());
        searchedTextField = new JTextField();
        searchedTextField.setEnabled(false);
        excludeCheckBox = new JCheckBox("Exclude");
        excludeCheckBox.setEnabled(false);
        conditionButtonAndLabelPanel.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        conditionButtonAndLabelPanel.add(BorderLayout.WEST, conditionCheckBox);
        conditionButtonAndLabelPanel.add(BorderLayout.CENTER, searchedTextField);
        conditionButtonAndLabelPanel.add(BorderLayout.EAST, excludeCheckBox);
        conditionButtonAndLabelPanel.setEnabled(false);
        // END conditional button and label panel


        // count button and CleanMetrics button panel
        JPanel buttonsPanel = new JPanel();
        JButton logButton = new JButton("Save log to file");
        logButton.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        logButton.addActionListener(new LogButtonClick());
        countLogButton = new JButton("Count");
        countLogButton.addActionListener(new LogButtonClick());
        buttonsPanel.add(countLogButton);
        buttonsPanel.add(logButton);
        // END log button and CleanMetrics button panel

        logPanel.add(testIDButtonAndLabelPanel);
        logPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        logPanel.add(conditionButtonAndLabelPanel);
        logPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        logPanel.add(buttonsPanel);
        /* ------- End log panel ---------- */

        // status panel
        JPanel statusPanel = new JPanel();
        statusPanel.setBorder(new EmptyBorder(10, 0, 0, 0));
        statusPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
        statusPanel.setPreferredSize(new Dimension(this.getWidth(), 20));
        statusPanel.setLayout(new BoxLayout(statusPanel, BoxLayout.X_AXIS));
        statusLabel = new JLabel("Status");
        statusLabel.setBorder(new EmptyBorder(0, 5, 0, 0));
        statusPanel.add(statusLabel);
        // end status panel

        // main panel and Frame
        mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout(0, 10));
        mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        mainPanel.add(BorderLayout.NORTH, cleanMetricPanel);
        mainPanel.add(logPanel);


        setLayout(new BorderLayout(0, 10));
        setJMenuBar(jMenuBar);
        add(mainPanel, BorderLayout.NORTH);
        add(statusPanel, BorderLayout.SOUTH);
        pack();
        setVisible(true);
        // end main panel and Frame

    }

    public void setStatus(String statusText) {
        statusLabel.setText(statusText);
    }

    private class ClearMetricButtonClick implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int numOfCleanRows = dbManager.cleanTable("metric");
            if (numOfCleanRows < 0)
                statusLabel.setText("WARNING: It is impossible to clean the metric table");
            else
                statusLabel.setText("Successfully: Metric was cleaned");
        }
    }


    private class LogButtonClick implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            boolean isIncludeSearchngText = true;

            if (!testIDField.getText().isEmpty()) {

                // set result
                if (conditionCheckBox.isSelected() && !searchedTextField.getText().isEmpty()) {
                    if (excludeCheckBox.isSelected())
                        isIncludeSearchngText = false;
                    result = dbManager.getLogWithParameters(testIDField.getText(), searchedTextField.getText(), isIncludeSearchngText);
                } else {
                    result = dbManager.getLog(testIDField.getText());
                }

                // check result
                if (result.getSize() == 0) {
                    setStatus("WARNING: Logs are empty or does not exist in DB");
                } else {
                    if(e.getSource() == countLogButton){
                        setStatus("Log contains: " + result.getSize() + " rows");
                    }else {
                        saveToFile(result);
                    }
                }
            } else {
                setStatus("WARNING: Fill in the Test ID field!");
            }
        }
    }

    private class ConditionCheckBoxClick implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (searchedTextField.isEnabled()) {
                searchedTextField.setEnabled(false);
                excludeCheckBox.setEnabled(false);
                excludeCheckBox.setSelected(false);
            } else {
                searchedTextField.setEnabled(true);
                excludeCheckBox.setEnabled(true);
            }
        }
    }

    private void saveToFile(ResponseHolder result) {
        try {
            JFileChooser chooser = new JFileChooser();
            String curStringTime = new SimpleDateFormat("HH_mm_ss").format(System.currentTimeMillis());
            if(Calendar.getInstance().getTimeZone().getDefault().getID().toString().equals("Europe/Moscow")) {
                curStringTime = new SimpleDateFormat("HH_mm_ss").format(System.currentTimeMillis()-3600000);
            }

            chooser.setSelectedFile(new File("log_" + testIDField.getText().substring(0, 4) + "_" + curStringTime));
            chooser.setCurrentDirectory(new File(Paths.get("").toAbsolutePath().toString()));
            int returnVal = chooser.showSaveDialog(this);
            if(returnVal == JFileChooser.APPROVE_OPTION){
                File file = chooser.getSelectedFile();
                if(!file.getName().contains(".txt"))
                    file = new File(file + ".txt");
                //System.out.println("Path: " + file.getAbsolutePath() + " File: " + file.getName());
                PrintWriter out = new PrintWriter(new FileOutputStream(file));
                out.print(result);
                out.close();
                setStatus("Successfully. File was created. Log contains: " + result.getSize() + " rows");
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            setStatus("WARNING: " + ex.getMessage());
        }

    }

    private class CleanRuntimeRequestButtonClick implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int numOfCleanRows = dbManager.cleanTable("runtime_request");
            if (numOfCleanRows < 0)
                statusLabel.setText("WARNING: It is impossible to clean the runtime_request table");
            else
                statusLabel.setText("Successfully: Runtime requests was cleaned");
        }
    }

}
