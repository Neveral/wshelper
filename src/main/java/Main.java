import gui.MainFrame;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    MainFrame.getInstance();
                }catch(Throwable ex){
                    ex.printStackTrace();
                    JPanel panel = new JPanel();
                    JOptionPane.showMessageDialog(panel, "There are some troubles with program", "Error", JOptionPane.ERROR_MESSAGE);
                    System.exit(0);
                }
            }
        });
    }
}
