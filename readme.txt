WSHelper configuration:
1) all database settings are kept in db.properties file
2) change database name if you need (by default: certperf)
3) change username and password to yours (by default: username = admin, password = password)
3) if you use MS SQL server change DB_DRIVER_CLASS to: com.microsoft.sqlserver.jdbc.SQLServerDriver

PS: the log file saves to current dir with name log.txt